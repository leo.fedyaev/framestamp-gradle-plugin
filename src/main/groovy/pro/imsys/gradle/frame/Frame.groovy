package pro.imsys.gradle.frame

/**
 * Since 1.2
 * Changed mechanism of field's replacement.
 * Now it uses simple string replacement functionality instead of xml.
 */
class Frame {
    String buffer

    Frame(String name) {
        buffer = convertStreamToString(getClass().getResourceAsStream("/resources/frames/${name}.svg"))
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A")
        return s.hasNext() ? s.next() : ""
    }

    private void setValue(String node, String value) {
        buffer = buffer.replace("##${node.toUpperCase()}##", value)
    }

    private void setValues(String node, String[] values) {
        for (int index = 0; index < values.length; index++)
            setValue(node + index, values[index])
    }

    Frame modify(Map values) {
        for (it in values) {
            (it.key != 'title' && (it.key != 'company')) ?
                    setValue(it.key as String, it.value.toString()) : setValues(it.key as String, it.value as String[])
        }

        this
    }

    boolean save(String svg, String pdf) {

        new File(svg).text = buffer

        def cmd = ['inkscape', svg, "--export-pdf=${pdf}"]

        if (0 != cmd.execute().waitFor()) {
            println "\t\tERROR: failed to convert from SVG to PDF."
            return false
        }

        true
    }
}
