package pro.imsys.gradle.task

import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

/**
 * Convert target DIA file to the corresponding PDF file.
 */
class DiaTask extends PluginTask {

    @TaskAction
    def action() {

        logger.lifecycle("\t[${domain()}]::${target().name} -->")

        outDir().mkdirs()

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('dia', '--filter', 'svg', target(), '-O', "${outDir().path}/")
        }

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('inkscape',
                    "${outDir().path}/${baseName(target().name)}.svg",
                    "--export-pdf=${outDir().path}/${baseName(target().name)}.raw.pdf")
        }

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('cpdf', '-scale-to-fit', '260mm 190mm',
                    "${outDir().path}/${baseName(target().name)}.raw.pdf",
                    '-o',
                    "${outDir().path}/${baseName(target().name)}.scale.pdf")
        }

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('cpdf', '-shift', '25mm 30mm',
                    "${outDir().path}/${baseName(target().name)}.scale.pdf",
                    '-o',
                    "${outDir().path}/${baseName(target().name)}.pdf")
        }

        logger.lifecycle("\t\t: converted;")
        logger.lifecycle("\t--> ${result().name}")
    }
}
