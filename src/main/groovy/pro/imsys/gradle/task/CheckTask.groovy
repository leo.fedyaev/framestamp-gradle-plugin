package pro.imsys.gradle.task

import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.api.DefaultTask
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction
import org.gradle.internal.os.OperatingSystem

/**
 * Check wherever required dependencies installed in the current system.
 */
class CheckTask extends DefaultTask {

    def final check = [
            soffice: ['which', 'soffice'],
            pdflatex: ['which', 'pdflatex'],
            inkscape: ['which', 'inkscape'],
            cpdf: ['which', 'cpdf']
    ]

    def final wincheck = [
            soffice: ['soffice', '--help'],
            pdflatex: ['pdflatex', '-help'],
            inkscape: ['inkscape', '--help'],
            cpdf: ['cpdf', '--help']
    ]

    @TaskAction
    def action() {
        logger.lifecycle("\tOS: ${OperatingSystem.current().getName()} ${OperatingSystem.current().getVersion()}\n")

        def deps = Os.isFamily(Os.FAMILY_UNIX) ? check : wincheck

        deps.each { name, args ->
            def result = project.exec {
                logging.captureStandardOutput(LogLevel.DEBUG)
                logging.captureStandardError(LogLevel.DEBUG)

                commandLine(args)
                ignoreExitValue = true
            }

            def title = "[${name}]".padRight(11, ' ')
            logger.lifecycle("\t${title}: ${result.exitValue != 0 ? '\033[0;31mfalse\033[0m' : 'true'};")
        }
    }
}