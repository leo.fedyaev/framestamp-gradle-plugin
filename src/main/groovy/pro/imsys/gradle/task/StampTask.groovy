package pro.imsys.gradle.task

import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction
import pro.imsys.gradle.frame.First
import pro.imsys.gradle.frame.Frame
import pro.imsys.gradle.frame.Landscape
import pro.imsys.gradle.frame.Mainland
import pro.imsys.gradle.frame.Portrait
import pro.imsys.gradle.frame.Title

import javax.print.Doc
import java.util.regex.Matcher

/**
 * Stamp every page of PDF file by special frame.
 */
class StampTask extends PluginTask {

    /* document types */
    enum DocType {
        SIMPLE_PORTRAIT, SIMPLE_LANDSCAPE, FULL
    }

    /* build directories */
    def final dirs = [
            pages: "${outDir()}/stamp/${project.name}/pages",
            frames: "${outDir()}/stamp/${project.name}/frames",
            stamps: "${outDir()}/stamp/${project.name}/stamps",
            src: "${outDir()}/stamp/${project.name}/frames/src"
    ]

    @TaskAction
    def action() {
        logger.lifecycle("\t[${domain()}]::${target().name} -->")

        /* delete stamp dir */
        new File("${outDir()}/stamp/${project.name}/").deleteDir()

        /* create dirs */
        dirs.each { key, value ->
            new File(value).mkdirs()
        }

        /* split target into pages */
        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('cpdf', '-split', target(), '-o', "${dirs.pages}/page%%%.pdf")
        }

        int count = new File(dirs.pages).listFiles().length

        DocType type = getDocType(count)
        logger.lifecycle("\t\t: splitted(pages = ${count}; type = ${type.toString()});")

        /* create frames */
        createFrames(type, count)

        logger.lifecycle("\t\t: created(frames = ${count});")

        /* stamp frames */
        for (int page = 0; page < count; page++) {
            def name = "page${num(page)}.pdf"

            project.exec {
                logging.captureStandardOutput(LogLevel.DEBUG)
                logging.captureStandardError(LogLevel.DEBUG)

                commandLine ('cpdf', '-stamp-on',
                        "${dirs.pages}/${name}", "${dirs.frames}/${name}", '-o', "${dirs.stamps}/${name}")
            }
        }

        logger.lifecycle("\t\t: stamped(pages = ${count});")

        /* merge pages together */
        String files = ''
        new File(dirs.stamps).listFiles()?.sort{a, b -> (a.name <=> b.name)}?.
                findAll { it.name.substring(it.name.lastIndexOf('.pdf'))}?.each {
            files += "${it.path.replace(' ', '\\ ')} "
        }

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine ('sh', '-c', "cpdf -merge ${files} -o '${result()}'")
        }

        logger.lifecycle("\t\t: merged(pages = ${count});")

        /* squeeze target */
        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine ('cpdf', '-squeeze', result(), '-o', result())
        }

        logger.lifecycle("\t\t: shrinked;")


        logger.lifecycle("\t--> ${result().name}")
    }

    /**
     *
     */
    @Override
    File target() {
        new File("${outDir()}/${baseName(project.name)}.pdf")
    }

    @Override
    File result() {
        new File("${outDir()}/${baseName(project.name)}-Framed.pdf")
    }

    /**
     * Create full stack of frames
     */
    protected void createFrames(DocType doctype, int count) {
        def type = getFilenamePattern(0)
        def index = "${getFilenamePattern(2)}.${getFilenamePattern(1)}"

        Map values = [
                index   : "${ext.index}.${index} ${type}",
                parent  : "${ext.index}.${index}",
                develop : ext.people[0],
                verify  : ext.people[1],
                control : ext.people[2],
                approval: ext.people[3],
                count   : count,
                company : ext.company,
                title   : [project.gradle.title, domain(),
                           (ext.types.containsKey(type) ?
                                   ext.types[type] : 'Неизвестный тип диаграммы')]
        ]

        for (int page = 0; page < count; page++) {
            if (doctype == DocType.SIMPLE_LANDSCAPE) {
                /* first page */
                if (page == 0) {
                    new Mainland()
                            .modify(values)
                            .save(svg(page), pdf(page))
                }
                /* another page */
                else {
                    new Landscape()
                            .modify([index: "${ext.index}.${index} ${type}", page: page + 1])
                            .save(svg(page), pdf(page))
                }
            }
            else if (doctype == DocType.SIMPLE_PORTRAIT) {
                /* first page */
                if (page == 0) {
                    new Title()
                            .modify([code: ''])
                            .save(svg(page), pdf(page))
                }
                /* another page */
                else {
                    new Portrait()
                            .modify([index: "${ext.index}.${index} ${type}", page: page + 1])
                            .save(svg(page), pdf(page))
                }
            }
            else {
                /* title frame */
                if (page == 0) {
                    new Title()
                            .modify([code: ''])
                            .save(svg(page), pdf(page))

                } else
                /* first page */
                if (page == 1) {
                    new First()
                            .modify(values)
                            .save(svg(page), pdf(page))
                }
                /* default page */
                else {
                    Frame frame = isLandscape("${dirs.pages}/page${num(page)}.pdf") ?
                            new Landscape() : new Portrait()
                    frame.modify([index: "${ext.index}.${index} ${type}", page: page + 1])
                            .save(svg(page), pdf(page))
                }
            }
        }
    }

    /**
     * What kind of orientation is this page?
     */
    boolean isLandscape(String filename) {

        def output = new StringBuilder()
        def process = ['cpdf', '-page-info', "${filename}"].execute()
        def ret = false

        process.waitForProcessOutput(output, System.err)
        output.eachLine { line ->
            Matcher matcher = line =~ /(?ms)(MediaBox:) (\d+.\d+) (\d+.\d+) (\d+.\d+) (\d+.\d+)/

            if (matcher) {
                double x = matcher[0][4].toDouble()
                double y = matcher[0][5].toDouble()

                if (x > y)
                    ret = true
            }
        }

        ret
    }

    /**
     * Is this document simple or full?
     * SIMPLE_? document contains only two pages.
     */
    DocType getDocType(int count) {
        int landscape, portrait = 0

        /* stamp frames */
        for (int page = 0; page < count; page++) {
            def name = "page${num(page)}.pdf"

            if (isLandscape("${dirs.pages}/${name}"))
                landscape += 1
            else
                portrait += 1
        }

        if (landscape == 2)
            return DocType.SIMPLE_LANDSCAPE
        else if (portrait == 2)
            return DocType.SIMPLE_PORTRAIT
        else
            return DocType.FULL
    }

    /**
     * Take a part of target filename
     */
    String getFilenamePattern(int group) {
        (project.name =~ ext.mask)[0][group + 1]
    }

    /**
     *
     */
    private String svg(int page) {
        "${dirs.src}/page${num(page)}.svg"
    }

    private String pdf(int page) {
        "${dirs.frames}/page${num(page)}.pdf"
    }

    private String num(int page) {
        "${page + 1}".padLeft(3, '0')
    }
}
