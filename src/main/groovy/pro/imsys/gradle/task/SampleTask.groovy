package pro.imsys.gradle.task

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files
import java.nio.file.Paths

/**
 * Create sample project to manually test plugin.
 */
class SampleTask extends DefaultTask {

    File sampleDir = project.gradle.sampleDir

    @TaskAction
    def action() {
        InputStream is = getClass().getResourceAsStream('/resources/sample/sample.zip')
        Files.copy(is, Paths.get("${sampleDir.path}/sample.zip"))

        File zip = new File("${sampleDir.path}/sample.zip")

        project.copy {
            from project.zipTree(zip)
            into sampleDir
        }

        zip.delete()

        logger.lifecycle("\t[MKDIR]: ${sampleDir.path} -->")

        sampleDir.eachFileRecurse {
            logger.lifecycle("\t\t ... ${it.path.substring(sampleDir.path.length() + 1)}")
        }
    }
}
