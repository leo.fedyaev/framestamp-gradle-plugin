package pro.imsys.gradle.task

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * Clean all build stuff located in '${buildDir}' directory of the root project.
 */
class CleanTask extends DefaultTask {
    File buildDir = project.gradle.buildDir
    File installDir = project.gradle.installDir
    File sampleDir = project.gradle.sampleDir

    @TaskAction
    def action() {

        buildDir.deleteDir()
        installDir.deleteDir()
        sampleDir.deleteDir()

        logger.lifecycle("\t[DEL]: ${buildDir}")
        logger.lifecycle("\t[DEL]: ${installDir}")
        logger.lifecycle("\t[DEL]: ${sampleDir}")
    }
}
