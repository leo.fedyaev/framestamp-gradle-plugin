package pro.imsys.gradle.task

import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

/**
 * Convert target TEX file to the corresponding PDF file.
 */
class LatexTask extends PluginTask {
    @TaskAction
    def action() {

        logger.lifecycle("\t[${domain()}]::${target().name} -->")

        outDir().mkdirs()

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('pdflatex', '-halt-on-error', '-output-directory', outDir().path, target())
        }

        logger.lifecycle("\t\t: converted;")
        logger.lifecycle("\t--> ${result().name}")
    }
}
