package pro.imsys.gradle.task

import org.gradle.api.DefaultTask

/**
 *
 */
abstract class PluginTask extends DefaultTask {
    File sourceDir = project.gradle.sourceDir
    File buildDir = project.gradle.buildDir
    File installDir = project.gradle.installDir
    def ext = project.rootProject.extensions.framestamp

    /**
     * Get target (input) file of current task.
     */
    File target() {
        new File("${project.projectDir}/${project.name}")
    }

    /**
     * Get result (output) file of current task.
     */
    File result() {
        new File("${outDir()}/${baseName(project.name)}.pdf")
    }

    /**
     * Get project's build directory.
     */
    File outDir() {
        new File("${project.rootDir}/${buildDir.name}/${qualifier()}")
    }

    /**
     * Get base name of file. Trim extension.
     */
    protected String baseName(String file) {
        file.substring(0, file.lastIndexOf('.'))
    }

    /**
     * Get project qualifier - parent directories name up to $outDir.
     */
    protected String qualifier() {
        project.projectDir.absolutePath.substring(project.rootDir.absolutePath.length() + sourceDir.name.length() + 1)
    }

    /**
     * Get domain name - one of the parent directories according project type convention.
     */
    protected String domain() {
        (new File(project.projectDir.path)).name
    }
}
