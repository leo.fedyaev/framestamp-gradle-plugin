package pro.imsys.gradle.task

import org.gradle.api.tasks.TaskAction

/**
 * Copy result file into '${installDir}' directory.
 */
class InstallTask extends PluginTask {

    @TaskAction
    def action() {

        logger.lifecycle("\t[${domain()}]::${target().name} -->")

        project.copy {
            from target()
            into outDir()

            rename { String name ->
                name.replace('-Framed', '')
            }
        }

        logger.lifecycle("\t\t: copied;")
        logger.lifecycle("\t--> ${result().name}")
    }

    /**
     *
     */

    @Override
    File target() {
        def name = "${project.rootDir}/${buildDir.name}/${qualifier()}/${baseName(project.name)}"
        def file = new File("${name}-Framed.pdf")

        if (!file.exists())
            file = new File("${name}.pdf")

        file
    }

    @Override
    File result() {
        new File("${outDir()}/${baseName(project.name)}.pdf")
    }

    @Override
    File outDir() {
        new File("${project.rootDir}/${installDir.name}/${domain()}")
    }
}
