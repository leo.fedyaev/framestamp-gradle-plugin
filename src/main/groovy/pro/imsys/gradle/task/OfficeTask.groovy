package pro.imsys.gradle.task

import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

/**
 * Convert target ODT file to the corresponding PDF file.
 */
class OfficeTask extends PluginTask {

    @TaskAction
    def action() {

        logger.lifecycle("\t[${domain()}]::${target().name} -->")

        outDir().mkdirs()

        project.exec {
            logging.captureStandardOutput(LogLevel.DEBUG)
            logging.captureStandardError(LogLevel.DEBUG)

            commandLine('soffice', '--headless', '--convert-to', 'pdf', target(), '--outdir', outDir().path)
        }

        logger.lifecycle("\t\t: converted;")
        logger.lifecycle("\t--> ${result().name}")
    }
}
