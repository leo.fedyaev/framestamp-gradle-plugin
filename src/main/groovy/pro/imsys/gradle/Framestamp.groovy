package pro.imsys.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import pro.imsys.gradle.task.CheckTask
import pro.imsys.gradle.task.CleanTask
import pro.imsys.gradle.task.InstallTask
import pro.imsys.gradle.task.OfficeTask
import pro.imsys.gradle.task.SampleTask
import pro.imsys.gradle.task.StampTask
import pro.imsys.gradle.task.LatexTask
import pro.imsys.gradle.task.DiaTask

/**
 *
 */
class Framestamp implements Plugin<Project> {
    @Override
    void apply(Project project) {
        /* register user extension */
        project.extensions.create('framestamp', Extension)

        /**
         * :pdf task
         */
        project.subprojects.findAll { it.name.endsWith('.odt') }.each {

            it.task('pdf', type: OfficeTask) { task ->
                group = 'framestamp'
                description = 'Convert target file to the corresponding PDF file.'

                inputs.file(task.target())
                outputs.file(task.result())
            }
        }

        project.subprojects.findAll { it.name.endsWith('.ods') }.each {

            it.task('pdf', type: OfficeTask) { task ->
                group = 'framestamp'
                description = 'Convert target file to the corresponding PDF file.'

                inputs.file(task.target())
                outputs.file(task.result())
            }
        }

        project.subprojects.findAll { it.name.endsWith('.tex') }.each {

            it.task('pdf', type: LatexTask) { task ->
                group = 'framestamp'
                description = 'Convert target file to the corresponding PDF file.'

                inputs.file(task.target())
                outputs.file(task.result())
            }
        }

        project.subprojects.findAll { it.name.endsWith('.dia') }.each {

            it.task('pdf', type: DiaTask) { task ->
                group = 'framestamp'
                description = 'Convert target file to the corresponding PDF file.'

                inputs.file(task.target())
                outputs.file(task.result())
            }
        }

        /**
         * :stamp and :install tasks
         */
        project.subprojects.each {

            it.task('stamp', type: StampTask) { task ->
                group = 'framestamp'
                description = 'Stamp every page of PDF file by special frame.'

                dependsOn('pdf')

                inputs.file(task.target())
                outputs.file(task.result())
            }

            it.task('install', type: InstallTask) { task ->
                group = 'framestamp'
                description = "Copy result file into '${task.installDir.name}' directory."

                dependsOn('pdf')

                inputs.file(task.target())
                outputs.file(task.result())
            }
        }

        /**
         * :clean task
         */
        project.task('clean', type: CleanTask) { task->
            group = 'framestamp'
            description = "Clean all build stuff located in '${task.buildDir.name}' directory of the root project."
        }

        /**
         * :check task
         */
        project.task('check', type: CheckTask) { task->
            group = 'framestamp'
            description = "Check wherever required dependencies installed in the current system."
        }

        /**
         * :sample task
         */
        project.task('sample', type: SampleTask) { task->
            group = 'framestamp'
            description = 'Create sample project to manually test plugin.'

            outputs.dir(task.sampleDir)
        }
    }
}
